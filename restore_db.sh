set -e

usage="$(basename "$0") [-h] [-d DB_NAME] [-D DB_BACKUP_DATE]
Restore a generic database saved at specific day
where:
    -h  show this help text
    -d  database name, eg. tsunami, seisev, mole, seisnet ...)
    -D  backup date: yyyy-mm-dd
    -c  container
    -e  exclude tables (list)
    -p  mysql root password"

options=':hD:d:c:e:p:'
while getopts $options option; do
  case "$option" in
    h) echo "$usage"; exit;;
    d) DB_NAME=$OPTARG;;
    D) DB_BACKUP_DATE=$OPTARG;;
    c) CONTAINER=$OPTARG;;
    e)
      EXCLUDED_TABLES+=("$OPTARG")
      while [ "$OPTIND" -le "$#" ] && [ "${!OPTIND:0:1}" != "-" ]; do
        EXCLUDED_TABLES+=("${!OPTIND}")
        OPTIND="$(expr $OPTIND \+ 1)"
      done
      ;;
    p) MYSQL_ROOT_PASSWORD=$OPTARG;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2; echo "$usage" >&2; exit 1;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2; echo "$usage" >&2; exit 1;;
  esac
done

# mandatory arguments
if [ ! "$DB_NAME" ] || [ ! "$DB_BACKUP_DATE" ] || [ ! "$CONTAINER" ] || [ ! "$MYSQL_ROOT_PASSWORD" ]; then
  echo "======================================================="
  echo "Missing al least one mandatory argument in: -d -D -c -p"
  echo "======================================================="
  echo "$usage" >&2; exit 1
fi


DB_DIR=/db_backup/${DB_BACKUP_DATE}/${DB_NAME}
EXCLUDED_TABLES="${EXCLUDED_TABLES[@]}"

echo ${DB_DIR}
echo EXCLUDED_TABLES="${EXCLUDED_TABLES}"

cat <<EOF | docker exec --interactive "${CONTAINER}" bash

export MYSQL_PWD=$MYSQL_ROOT_PASSWORD

if [ ! -d ${DB_DIR} ]; then
  echo "Directory ${DB_DIR} do not exists."
  exit 1
fi

mysql -Nse 'DROP database IF EXISTS ${DB_NAME};'
mysql -Nse 'CREATE database ${DB_NAME};'

gunzip < ${DB_DIR}/${DB_NAME}.skt.gz | mysql -D ${DB_NAME}
gunzip < ${DB_DIR}/${DB_NAME}.triggers.gz | mysql -D ${DB_NAME}

mysql -Nse "SELECT table_name FROM information_schema.tables WHERE table_type = 'BASE TABLE' AND TABLE_SCHEMA='${DB_NAME}'" -D ${DB_NAME} > /tmp/.tables
while read table
do

  if [[ " ${EXCLUDED_TABLES} " =~ .*\ \${table}\ .* ]]; then
    echo "table \${table} EXCLUDED"
    continue
  fi

	gunzip  < ${DB_DIR}/${DB_NAME}-\${table}.dmp.gz | mysql -D ${DB_NAME};
	echo "table \${table} restored"
done < /tmp/.tables

EOF
