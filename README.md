
## Description

This project permits to replicate a generic mysql database of INGV on docker container, restoring it from a backup saved at generic day.

INGV database are saved, each day, at: 

```
hdbbck-rm.int.ingv.it:/mnt/dbbackup/adsDBbkup/hdbrm/__DB_NAME__/YYY-MM-DD
```


## Quick start

```
git clone https://gitlab.rm.ingv.it/docker/docker-dbsala.git
```
```
cd docker-dbsala
```
```
cp .env_template .env
```

```
sshfs root@hdbbck-rm.int.ingv.it:/mnt/dbbackup/adsDBbkup/hdbrm /YOUR/LOCAL/FOLDER -o allow_other,default_permissions
```

at this point inside the path /YOUR/LOCAL/FOLDER you will see the INGV backuped database in this form:

```
/YOUR/LOCAL/FOLDER/tsunami/YYYY-MM-DD
/YOUR/LOCAL/FOLDER/seisev/YYYY-MM-DD
etc ..
```

set the variable `BKP_DIRECTORY`:

```
BKP_DIRECTORY=/YOUR/LOCAL/FOLDER
```

Finally you can start the container 

```
docker-compose up -d
```

create your database user:

```
./create_user.sh -u dbuser -p password
```

Connect to mysql providing the correct password, that in this case is:  `password`:

```
mysql -h your_machine_name -P 3307 -u dbuser -p
```



### Restore a database

Choose a database (eg tsunami, mole, seisev, seisnet etc ...), a generic backup-day, and then run the following script:

```
./restore_db.sh -d tsunami -D 2024-06-04
```

After you have restored your/s database/s you can `unmount` you local backup folder:

```
umount -l /YOUR/LOCAL/FOLDER
```

### Init database tsunami 

Only for database `tsunami` the image contains a backup with principals tables already initialized with data 
When the container is started you can initialize the `tsunami` db with the following command:
```
docker exec -it tsunamidb bash -c "init_tsunamidb"
```

happy work!
