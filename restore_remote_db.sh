set -e

usage="$(basename "$0") [-h] [-d DB_NAME] [-D DB_BACKUP_DATE]
Restore a generic database saved at specific day
where:
    -h  show this help text
    -d  database name, eg. tsunami, seisev, mole, seisnet ...)
    -D  backup date: yyyy-mm-ddr
    -b  local database backup folder
    -e  exclude tables (list)
    -H  mysql host
    -P  mysql port
    -u  mysql user
    -p  mysql password"

options=':hD:d:b:H:P:u:p:e:'
while getopts $options option; do
  case "$option" in
    h) echo "$usage"; exit;;
    d) DB_NAME=$OPTARG;;
    D) DB_BACKUP_DATE=$OPTARG;;
    b) DB_BACKUP_FOLDER=$OPTARG;;
    e)
      EXCLUDED_TABLES+=("$OPTARG")
      while [ "$OPTIND" -le "$#" ] && [ "${!OPTIND:0:1}" != "-" ]; do
        EXCLUDED_TABLES+=("${!OPTIND}")
        OPTIND="$(expr $OPTIND \+ 1)"
      done
      ;;

    H) MYSQL_HOST=$OPTARG;;
    P) MYSQL_PORT=$OPTARG;;
    u) MYSQL_USER=$OPTARG;;
    p) MYSQL_PASSWORD=$OPTARG;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2; echo "$usage" >&2; exit 1;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2; echo "$usage" >&2; exit 1;;
  esac
done

# mandatory arguments
if [ ! "$DB_NAME" ]; then
  echo "argument -d"
  echo "$usage" >&2; exit 1
fi

if [ ! "$DB_BACKUP_DATE" ]; then
  echo "argument -D must be provided"
  echo "$usage" >&2; exit 1
fi

if [ ! "$DB_BACKUP_FOLDER" ]; then
  echo "argument -b must be provided"
  echo "$usage" >&2; exit 1
fi

if [ ! "$MYSQL_HOST" ]; then
  echo "argument -H must be provided"
  echo "$usage" >&2; exit 1
fi

if [ ! "$MYSQL_PORT" ]; then
  echo "argument -P must be provided"
  echo "$usage" >&2; exit 1
fi

if [ ! "$MYSQL_USER" ]; then
  echo "argument -u must be provided"
  echo "$usage" >&2; exit 1
fi

if [ ! "$MYSQL_PASSWORD" ]; then
  echo "argument -p must be provided"
  echo "$usage" >&2; exit 1
fi

DB_DIR="${DB_BACKUP_FOLDER}/${DB_BACKUP_DATE}/${DB_NAME}"

EXCLUDED_TABLES="${EXCLUDED_TABLES[@]}"

echo DB_DIR="${DB_DIR}"
echo DB_NAME="${DB_NAME}"
echo MYSQL_HOST="${MYSQL_HOST}"
echo MYSQL_PORT="${MYSQL_PORT}"
echo MYSQL_USER="${MYSQL_USER}"
echo MYSQL_PASSWORD="${MYSQL_PASSWORD}"
echo EXCLUDED_TABLES="${EXCLUDED_TABLES}"

export MYSQL_PWD=$MYSQL_PASSWORD

if [ ! -d ${DB_DIR} ]; then
  echo "Directory ${DB_DIR} do not exists."
  exit 1
fi

echo "DROP DATABSE..."
mysql "-h${MYSQL_HOST}" "-u${MYSQL_USER}" "-P${MYSQL_PORT}" -Nse "DROP database IF EXISTS ${DB_NAME};"

echo "CREATE DATABSE..."
mysql "-h${MYSQL_HOST}" "-u${MYSQL_USER}" "-P${MYSQL_PORT}" -Nse "CREATE database ${DB_NAME};"

echo "RESTORE SCHEMA..."
gunzip < "${DB_DIR}/${DB_NAME}.skt.gz" | mysql "-h${MYSQL_HOST}" "-u${MYSQL_USER}" "-P${MYSQL_PORT}" -D "${DB_NAME}"

echo "RESTORE TRIGGERS..."
gunzip < "${DB_DIR}/${DB_NAME}.triggers.gz" | mysql "-h${MYSQL_HOST}" "-u${MYSQL_USER}" "-P${MYSQL_PORT}" -D "${DB_NAME}"

echo "RESTORE TABLES..."
mysql "-h${MYSQL_HOST}" "-u${MYSQL_USER}" "-P${MYSQL_PORT}" -Nse "SELECT table_name FROM information_schema.tables WHERE table_type = 'BASE TABLE' AND TABLE_SCHEMA='${DB_NAME}'" -D "${DB_NAME}" > /tmp/.tables
while read table
do
	#echo "restoring table ${table} ...";

	if [[ " $EXCLUDED_TABLES " =~ .*\ $table\ .* ]]; then
		echo "table ${table} EXCLUDED"
		continue
	fi

	gunzip  < "${DB_DIR}/${DB_NAME}-${table}.dmp.gz" | mysql "-h${MYSQL_HOST}" "-u${MYSQL_USER}" "-P${MYSQL_PORT}" -D "${DB_NAME}"
	echo "table ${table} restored"
done < /tmp/.tables
