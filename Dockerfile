FROM mysql:5.7

COPY ./tsunami.sql.gz /usr/etc
COPY init_tsunamidb /usr/bin
