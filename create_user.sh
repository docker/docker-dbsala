set -e
usage="$(basename "$0") [-h] [-u USER] [-p PASSWORD]
Add generic user to the database
where:
    -h  show this help text
    -u  user name
    -p  password
    -c  container"

options=':hu:p:c:'

while getopts $options option; do
  case "$option" in
    h) echo "$usage"; exit;;
    u) USER=$OPTARG;;
    p) PASSWORD=$OPTARG;;
    c) CONTAINER=$OPTARG;;
    :) printf "missing argument for -%s\n" "$OPTARG" >&2; echo "$usage" >&2; exit 1;;
   \?) printf "illegal option: -%s\n" "$OPTARG" >&2; echo "$usage" >&2; exit 1;;
  esac
done

# mandatory arguments
#if [ ! "${USER}" ] || [ ! "{$PASSWORD}"]; then
#  echo "arguments -d and -p must be provided"
#  echo "$usage" >&2; exit 1
#fi

cat <<EOF | docker exec --interactive "${CONTAINER}" bash
mysql -Nse "DROP USER IF EXISTS '${USER}'@'%'";
mysql -Nse "CREATE USER '${USER}'@'%' IDENTIFIED BY '${PASSWORD}'";
mysql -Nse "GRANT ALL ON *.* TO '${USER}'@'%'; flush privileges;"
EOF
