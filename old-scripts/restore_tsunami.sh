cat <<EOF | docker exec --interactive dbrome bash
mysql -Nse 'DROP database IF EXISTS tsunami;'
mysql -Nse 'CREATE database tsunami;'
cd /data/tsunami_bkp
gunzip < tsunami.skt.gz | mysql -D tsunami
gunzip < tsunami.triggers.gz | mysql -D tsunami
mysql -Nse "SELECT table_name FROM information_schema.tables WHERE table_type = 'BASE TABLE' AND TABLE_SCHEMA='tsunami'" -D tsunami | while read table; do echo \$(date +"%Y-%m-%d %H:%M:%S") restoring table \${table} ...; gunzip  < tsunami-\${table}.dmp.gz | mysql -D tsunami; echo \$(date +"%Y-%m-%d %H:%M:%S") table \${table} restored; done
EOF
