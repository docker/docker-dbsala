cat <<EOF | docker exec --interactive dbrome bash
mysql -Nse 'DROP database IF EXISTS seisev;'
mysql -Nse 'CREATE database seisev;'
mysql -Nse "GRANT ALL ON seisev.* TO 'dbuser'@'%'; flush privileges;"
cd /data/seisev_bkp
gunzip < seisev.skt.gz | mysql -D seisev
mysql -Nse "SELECT table_name FROM information_schema.tables WHERE table_type = 'BASE TABLE' AND TABLE_SCHEMA='seisev'" -D seisev | while read table; do echo \$(date +"%Y-%m-%d %H:%M:%S") restoring table \${table} ...; gunzip  < seisev-\${table}.dmp.gz | mysql -D seisev; echo \$(date +"%Y-%m-%d %H:%M:%S") table \${table} restored; done
gunzip < seisev.triggers.gz | mysql -D seisev
EOF
