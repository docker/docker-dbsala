cat <<EOF | docker exec --interactive dbrome bash
mysql -Nse 'DROP database IF EXISTS mole;'
mysql -Nse 'CREATE database mole;'
cd /data/mole_bkp
gunzip < mole.skt.gz | mysql -D mole
gunzip < mole.triggers.gz | mysql -D mole
mysql -Nse "SELECT table_name FROM information_schema.tables WHERE table_type = 'BASE TABLE' AND TABLE_SCHEMA='mole'" -D mole | while read table; do echo \$(date +"%Y-%m-%d %H:%M:%S") restoring table \${table} ...; gunzip  < mole-\${table}.dmp.gz | mysql -D mole; echo \$(date +"%Y-%m-%d %H:%M:%S") table \${table} restored; done
EOF
